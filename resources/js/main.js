$(document).ready(function(){
	//Misitup (Portfolio)
    var containerEL = document.querySelector('.container');
	var mixer = mixitup(containerEL);

	// Sticky Menu
	$('.js--services-area').waypoint(function(direction){
		if (direction == "down") {
			$("nav").addClass("sticky");
		} else {
			$("nav").removeClass("sticky");
		}
	});

});

function openNav() {
	document.getElementById("myNav").style.width="100%";
}
function closeNav() {
	document.getElementById("myNav").style.width="0%";
}